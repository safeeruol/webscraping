import scrapy

class BahriaSpider(scrapy.Spider):
    name = 'lahore'
    allowed_domains = ['www.zameen.com']

    start_urls = ['https://www.zameen.com/Homes/Lahore_Bahria_Town-509-1.html']
    def start_requests(self):
        for x in range(1,1200):
            yield scrapy.Request(
                url=f'https://www.zameen.com/Homes/Lahore-1-{x}.html',
                callback=self.parse
            )

    def parse(self, response):
        list=response.xpath("//li[@aria-label='Listing']")
        for item in list:
            page_url = response.request.url
            url = item.xpath(".//a[@aria-label='Listing link']/@href").get()
            title=item.xpath(".//h2[@aria-label='Title']/text()").get()
            price=item.xpath(".//span[@aria-label='Price']/text()").get()
            location=item.xpath(".//div[@aria-label='Location']/text()").get()
            bed=item.xpath(".//span[@aria-label='Beds']/text()").get()
            bath=item.xpath(".//span[@aria-label='Baths']/text()").get()
            area=item.xpath(".//span[@aria-label='Area']/div/div/div/span/text()").get()

            yield {
                'price': price,
                'bed': bed,
                'bath': bath,
                'area': area,
                'title': title,
                'location': location,
                'page_url':page_url,
                'url':url
            }
