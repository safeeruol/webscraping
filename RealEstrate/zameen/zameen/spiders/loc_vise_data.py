import scrapy


class LocViseDataSpider(scrapy.Spider):
    name = 'complete_data'
    start_urls = ['https://www.zameen.com/all-cities/pakistan-1-9.html']

    def parse(self,response):
        print('----start parse-----')
        cities_list_url =response.xpath("//li[@class='_45afd756']/a")
        for city in cities_list_url:
            #/Houses_Property/Kotli-968-1.html
            city_property_list_url = city.xpath(".//@href").get()
            print(city_property_list_url)
            yield scrapy.Request(
                url='https://www.zameen.com' + city_property_list_url,
                callback=self.parse_property_list_per_page,
            )


    def parse_property_list_per_page(self, response):
        print('----start parse_property_list_per_page-----')
        list = response.xpath("//li[@aria-label='Listing']")
        for item in list:
            url = item.xpath(".//a[@aria-label='Listing link']/@href").get()
            title = item.xpath(".//h2[@aria-label='Title']/text()").get()

            print('propoerty item Url:'+url)
            # yield scrapy.Request(
            #     url='https://www.zameen.com'+url,
            #     callback=self.parse_property_details,
            #     meta = {
            #             'title': ''
            #             }
            # )

        next_page = response.xpath("//a[@title='Next']/@href").get()
        print(next_page)
        if next_page:
            yield scrapy.Request(
                url='http://zameen.com'+ next_page,
                callback=self.parse_property_list_per_page
            )


    def parse_property_details(self,response):

        # title = response.meta['title']
        url=response.request.url
        type=response.xpath("//span[@aria-label='Type']/text()").get()
        area = response.xpath("//span[@aria-label='Area']/span/text()").get().split(' ')[0]
        area_unit = response.xpath("//span[@aria-label='Area']/span/text()").get().split(' ')[-1]
        price = response.xpath("//span[@aria-label='Price']/text()").get()
        price_unit = response.xpath("//span[@aria-label='Price']/text()").get().split(' ')[-1]
        purpose=response.xpath("//span[@aria-label='Purpose']/text()").get()
        location=response.xpath("//span[@aria-label='Location']/text()").get()
        bed=response.xpath("//span[@aria-label='Beds']/text()").get()
        bath=response.xpath("//span[@aria-label='Baths']/text()").get()
        description=response.xpath("//div[@aria-label='Property description'] /div/span/text()[1]").get()

        yield {

            'type': type,
            'purpose': purpose,
            'price': price,
            'price_unit': price_unit,
            'bed': bed,
            'bath': bath,
            'area': area,
            'area_unit': area_unit,

            'location': location,
            'description': description,
            'url':url
        }