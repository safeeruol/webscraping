import scrapy


class PindiSpider(scrapy.Spider):
    name = 'pindi'
    allowed_domains = ['www.zameen.com']
    start_urls = ['https://www.zameen.com/all-cities/pakistan-1-9.html']

    def parse(self, response):
        list = response.xpath("//li[@class='_45afd756']/a")
        for item in list:
            url = item.xpath(".//@href").get()
            next_city_url = response.urljoin(url)
            yield scrapy.Request(
                url=next_city_url,
                callback=self.parse_page,
                meta={
                    'page_number': 1
                }
            )

    def parse_page(self,response):
        print('Page Status: '+str(response.status))

        #Page stat
        property_count_per_city=response.xpath('//h1/text()').get()
        count=response.xpath('//h1/text()').get().split(' ')[0].replace(',','')
        from_count=response.xpath("//span[@aria-label='Summary text']/text()").get().split(' ')[0]
        to_count=response.xpath("//span[@aria-label='Summary text']/text()").get().split(' ')[2]

        #property list
        list = response.xpath("//li[@aria-label='Listing']")
        for item in list:

            title = item.xpath(".//h2[@aria-label='Title']/text()").get()
            price = item.xpath(".//span[@aria-label='Price']/text()").get()
            price_amount = item.xpath(".//span[@aria-label='Price']/text()").get().split(' ')[0]
            price_unit = item.xpath(".//span[@aria-label='Price']/text()").get().split(' ')[-1]
            location = item.xpath(".//div[@aria-label='Location']/text()").get()
            bed = item.xpath(".//span[@aria-label='Beds']/text()").get()
            bath = item.xpath(".//span[@aria-label='Baths']/text()").get()
            area = item.xpath(".//span[@aria-label='Area']/div/div/div/span/text()").get()
            area_count = item.xpath(".//span[@aria-label='Area']/div/div/div/span/text()").get().split(' ')[0]
            area_unit = item.xpath(".//span[@aria-label='Area']/div/div/div/span/text()").get().split(' ')[-1]
            url = item.xpath(".//a[@aria-label='Listing link']/@href").get()
            property_item_url = response.urljoin(url)
            yield {
                'page_number': response.meta['page_number'],
                'page_url': response.url,
                'property_count_per_city': property_count_per_city,
                'count': count,
                'from': from_count,
                'to': to_count,
                'price': price,
                'price_amount': price_amount,
                'price_unit': price_unit,
                'bed': bed,
                'bath': bath,
                'area': area,
                'area_count': area_count,
                'area_unit': area_unit,
                'title': title,
                'location': location,
                'property_item_url': property_item_url
            }
        # yield {
        #     'page_number':response.meta['page_number'],
        #     'url':response.url,
        #     'property_count_per_city':property_count_per_city,
        #     'count':count,
        #     'from':from_count,
        #     'to':to_count
        # }

        url_parts = response.url.split('-')
        combile_url = '-'.join(url_parts[:-1])
        page_number = response.meta['page_number']
        page_number = page_number + 1
        next_page_url = combile_url + '-' + str(page_number) + '.html'

        if str(response.status)!='404':
            yield scrapy.Request(
                url=next_page_url,
                callback=self.parse_page,
                meta={
                    'page_number': page_number
                }
            )