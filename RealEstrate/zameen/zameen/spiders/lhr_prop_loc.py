import scrapy


class LhrPropLocSpider(scrapy.Spider):
    name = 'lhr_prop_loc'
    allowed_domains = ['zameen.com']
    start_urls = ['https://www.zameen.com/all_locations/Lahore-1-1-1.html']

    def parse(self, response):
        list=response.xpath("//ul[@class='line-list']/li")
        for item in list:
            url=item.xpath(".//a/@href").get()
            description=item.xpath(".//a/text()").get()
            location_count=item.xpath(".//span/text()").get().replace('(','').replace(')','').replace(',','')

            yield {
                'url': url,
                'description': description,
                'location_count': location_count,
            }
