import scrapy


class PkrPropLocSpider(scrapy.Spider):
    name = 'pkr_prop_loc'
    allowed_domains = ['zameen.com']
    start_urls = ['https://www.zameen.com/all-cities/pakistan-1-9.html']

    def parse(self, response):
        list = response.xpath("//li[@class='_45afd756']/a")
        for item in list:
            url = item.xpath(".//@href").get()
            description = item.xpath(".//@title").get()
            location_count = item.xpath(".//span/text()").get().replace('(', '').replace(')', '').replace(',', '')

            yield {
                'url': url,
                'description': description,
                'location_count': location_count,
            }