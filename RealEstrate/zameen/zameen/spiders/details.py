import scrapy


class DetailsSpider(scrapy.Spider):
    name = 'details'
    allowed_domains = ['zameen.com']

    start_urls = ['https://www.zameen.com/Homes/Lahore_Bahria_Town-509-1.html']

    def start_requests(self):
        for x in range(1,1200):
            yield scrapy.Request(
                url=f'https://www.zameen.com/Homes/Lahore-1-{x}.html',
                callback=self.parse
            )

    def parse(self, response):
        list = response.xpath("//li[@aria-label='Listing']")
        for item in list:
            url = item.xpath(".//a[@aria-label='Listing link']/@href").get()
            title = item.xpath(".//h2[@aria-label='Title']/text()").get()
            yield scrapy.Request(
                url='http://zameen.com'+url,
                callback=self.parse_details,
                meta = {
                    'title': title
                    }
            )

    def parse_details(self,response):
        # id = response.xpath("//div[@aria-label='Breadcrumb']/span[@aria-label='Link name']/text()").get().replace('House','').replace('Flat','').replace(' ','')
        # city = response.xpath("//div[@aria-label='Breadcrumb']/a[2]/span/text()").get().rsplit(' ', 1)[0]
        # town = response.xpath("//div[@aria-label='Breadcrumb']/a[3]/span/text()").get().rsplit(' ', 1)[0]
        # phase = response.xpath("//div[@aria-label='Breadcrumb']/a[4]/span/text()").get().rsplit(' ', 1)[0]
        # block = response.xpath("//div[@aria-label='Breadcrumb']/a[5]/span/text()").get().rsplit(' ', 1)[0]
        title = response.meta['title']
        url=response.request.url
        type=response.xpath("//span[@aria-label='Type']/text()").get()
        area=response.xpath("//span[@aria-label='Area']/span/text()").get().split(' ')[0]
        area_unit=response.xpath("//span[@aria-label='Area']/span/text()").get().split(' ')[-1]
        price=response.xpath("//span[@aria-label='Price']/text()").get()
        price_unit=response.xpath("//span[@aria-label='Price']/text()").get().split(' ')[-1]
        purpose=response.xpath("//span[@aria-label='Purpose']/text()").get()
        location=response.xpath("//span[@aria-label='Location']/text()").get()
        bed=response.xpath("//span[@aria-label='Beds']/text()").get().split(' ')[0]
        bath=response.xpath("//span[@aria-label='Baths']/text()").get().split(' ')[0]
        description=response.xpath("//div[@aria-label='Property description'] /div/span/text()[1]").get()

        yield {
            # 'id': id,
            # 'city': city,
            # 'town': town,
            # 'phase': phase,
            # 'block': block,
            'type': type,
            'purpose': purpose,
            'price': price,
            'price_unit': price_unit,
            'bed': bed,
            'bath': bath,
            'area': area,
            'area_unit': area_unit,
            'title': title,
            'location': location,
            'description': description,
            'url':url
        }

