import scrapy


class PakBasicSpider(scrapy.Spider):
    name = 'pak_basic'
    allowed_domains = ['zameen.com']
    start_urls = ['https://www.zameen.com/all-cities/pakistan-1-9.html']


    def parse(self, response):

        cities_list_url = response.xpath("//li[@class='_45afd756']/a")
        for city in cities_list_url:
            # /Houses_Property/Kotli-968-1.html
            city_property_list_url = city.xpath(".//@href").get()
            print(city_property_list_url)
            yield scrapy.Request(
                url='https://www.zameen.com' + city_property_list_url,
                callback=self.parse_property_list_per_page
            )

    def parse_property_list_per_page(self, response):
        print('----start parse_property_list_per_page-----')
        list = response.xpath("//li[@aria-label='Listing']")
        for item in list:
            url = item.xpath(".//a[@aria-label='Listing link']/@href").get()
            title = item.xpath(".//h2[@aria-label='Title']/text()").get()
            price = item.xpath(".//span[@aria-label='Price']/text()").get()
            location = item.xpath(".//div[@aria-label='Location']/text()").get()
            bed = item.xpath(".//span[@aria-label='Beds']/text()").get()
            bath = item.xpath(".//span[@aria-label='Baths']/text()").get()
            area = item.xpath(".//span[@aria-label='Area']/div/div/div/span/text()").get()

            yield {
                'price': price,
                'bed': bed,
                'bath': bath,
                'area': area,
                'title': title,
                'location': location,
                'url':url
            }
        next_page = response.xpath("//a[@title='Next']/@href").get()
        print(next_page)
        if next_page:
            yield scrapy.Request(
                url='http://zameen.com' + next_page,
                callback=self.parse_property_list_per_page
            )