import scrapy


class FromfileSpider(scrapy.Spider):
    name = 'fromfile'
    allowed_domains = ['test']
    start_urls = ['file:///E:/WebScrapingRepo/RealEstrate/centris/index.html']

    def parse(self, response):
        listings = response.xpath("//div[@class='description']")

        for item in listings:
            price = item.xpath(".//div[@class='price']/text()").get()
            yield {
                'price': price
            }
# test=item.xpath("//div[1]/div/a/div[1]/div[2]/meta[2]/@content").get()
