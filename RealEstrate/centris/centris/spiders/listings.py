import scrapy
import json
from scrapy.selector import  Selector

class ListingsSpider(scrapy.Spider):
    name = 'listings'
    allowed_domains = ['www.centris.ca']
    query={
        "query":{
            "UseGeographyShapes":0,
            "Filters":[

            ],
            "FieldsValues":[
                {
                    "fieldId":"Category",
                    "value":"Residential",
                    "fieldConditionId":"",
                    "valueConditionId":""
                },
                {
                    "fieldId":"SellingType",
                    "value":"Sale",
                    "fieldConditionId":"",
                    "valueConditionId":""
                },
                {
                    "fieldId":"LandArea",
                    "value":"SquareFeet",
                    "fieldConditionId":"IsLandArea",
                    "valueConditionId":""
                },
                {
                    "fieldId":"SalePrice",
                    "value":475000,
                    "fieldConditionId":"ForSale",
                    "valueConditionId":""
                },
                {
                    "fieldId":"SalePrice",
                    "value":1750000,
                    "fieldConditionId":"ForSale",
                    "valueConditionId":""
                }
            ]
        },
        "isHomePage":False
    }
    position={
        "startPosition":0
    }
    def start_requests(self):
        print('start_requests UpdateQuery')
        yield scrapy.Request(
            url="https://www.centris.ca/property/UpdateQuery",
            method="POST",
            body=json.dumps(self.query),
            headers={
                'Content-Type': 'application/json'
            },
            callback=self.update_query
        )

    def update_query(self, response):
        print(' UpdateQuery')
        resp_dict = json.loads(response.body)
        html = resp_dict.get('d').get('Result')
        print(html)

        yield scrapy.Request(
            url="https://www.centris.ca/Property/GetInscriptions",
            method="POST",
            body=json.dumps(self.position),
            headers={
                'Content-Type': 'application/json'
            },
            callback=self.parse
        )
    def parse(self, response):
        resp_dict=json.loads(response.body)
        html=resp_dict.get('d').get('Result').get('html')
        count=resp_dict.get('d').get('Result').get('count')
        increment_number=resp_dict.get('d').get('Result').get('inscNumberPerPage')


        # str_en = html.encode("ascii", "ignore")
        # str_de = str_en.decode()
        # clean_html="".join(str_de.split())

        # with open('index2.html', 'w') as f:
        #  f.write(html)

        list=Selector(text=html).xpath("//div[@class='description']")
        for item in list:
            print(item)


        # listings = Selector(text=clean_html).xpath("//div[@class='description']")
        # print(listings)
        # for item in listings:
        #     print(item)
        #     print('--------------------------------------------------------------')
        #     yield {
        #          'price':item
        #      }
        #
        # with open(str(self.position['startPosition'])+'index.html','w') as f:
        #     f.write(html)

        # print(increment_number)
        # print(count)
        # print(self.position['startPosition'])
        #
        # if self.position['startPosition']<=count:
        #     self.position['startPosition']+=increment_number
        #     yield scrapy.Request(
        #         url="https://www.centris.ca/Property/GetInscriptions",
        #         method="POST",
        #         body=json.dumps(self.position),
        #         headers={
        #             'Content-Type': 'application/json'
        #         },
        #         callback=self.parse
        #     )