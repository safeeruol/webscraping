import scrapy
import  json

class ListSpider(scrapy.Spider):
    name = 'list'
    allowed_domains = ['www.centris.ca']
    start_urls = ['http://www.centris.ca/']

    query = {
        "query": {
            "UseGeographyShapes": 0,
            "Filters": [

            ],
            "FieldsValues": [
                {
                    "fieldId": "Category",
                    "value": "Residential",
                    "fieldConditionId": "",
                    "valueConditionId": ""
                },
                {
                    "fieldId": "SellingType",
                    "value": "Sale",
                    "fieldConditionId": "",
                    "valueConditionId": ""
                },
                {
                    "fieldId": "LandArea",
                    "value": "SquareFeet",
                    "fieldConditionId": "IsLandArea",
                    "valueConditionId": ""
                },
                {
                    "fieldId": "SalePrice",
                    "value": 475000,
                    "fieldConditionId": "ForSale",
                    "valueConditionId": ""
                },
                {
                    "fieldId": "SalePrice",
                    "value": 1750000,
                    "fieldConditionId": "ForSale",
                    "valueConditionId": ""
                }
            ]
        },
        "isHomePage": False
    }
    position = {
        "startPosition": 0
    }

    def start_requests(self):
        print('start_requests UpdateQuery')
        yield scrapy.Request(
            url="https://www.centris.ca/property/UpdateQuery",
            method="POST",
            body=json.dumps(self.query),
            headers={
                'Content-Type': 'application/json'
            },
            callback=self.update_query
        )

    def update_query(self, response):
        print(' UpdateQuery')
        resp_dict = json.loads(response.body)
        html = resp_dict.get('d').get('Result')
        print(html)

        yield scrapy.Request(
            url="https://www.centris.ca/Property/GetInscriptions",
            method="POST",
            body=json.dumps(self.position),
            headers={
                'Content-Type': 'application/json'
            },
            callback=self.parse
        )

    def parse(self, response):
        print(response.body)