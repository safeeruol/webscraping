import requests
from bs4 import BeautifulSoup
import pandas as pd

def get_soup(url):
    r=requests.get('http://localhost:8050/render.html',params={'url':url, 'wait':2})
    soup=BeautifulSoup(r.text,'html.parser')
    return soup

reviewlist=[]
def get_reviews(soup):
    reviews=soup.find_all('div',{'data-hook':'review'})
    try:
        for item in reviews:
            review={
                'title': item.find('a', {'data-hook': 'review-title'}).text.strip(),
                'rating':  float(item.find('i', {'data-hook': 'review-star-rating'}).text.replace('out of 5 stars', '').strip()),
                'body': item.find('span', {'data-hook': 'review-body'}).text.strip(),
            }
            print(len(reviewlist))
            print(review)
            reviewlist.append(review)
    except:
        pass

for x in range(1,5):
    soup=get_soup(f'https://www.amazon.com/AmazonBasics-Gaming-Recliner-Headrest-Pillow/product-reviews/B07TJWWPJS/ref=cm_cr_arp_d_paging_btm_next_2?ie=UTF8&reviewerType=all_reviews&pageNumber={x}')
    print(f'Getting page {x}')
    get_reviews(soup)
    if not soup.find('li',{'class','a-last`'}):
        pass
    else:
        break

df=pd.DataFrame(reviewlist)
df.to_csv('sofa1.csv')
