from requests_html import HTMLSession
import pandas as pd
urls=['https://www.amazon.com/HyperX-Pulsefire-Haste-Ultra-Lightweight-Programmable/dp/B08NSJFNSS/ref=sr_1_2_sspa?keywords=gaming%2Bmouse&pd_rd_r=073adc02-9e0d-4826-a881-ef5b23617940&pd_rd_w=DxPBn&pd_rd_wg=jxPnR&pf_rd_p=12129333-2117-4490-9c17-6d31baf0582a&pf_rd_r=F535PWX760339489MFWX&qid=1650030224&sr=8-2-spons&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUEySEpGTkQ3SVZUTU1EJmVuY3J5cHRlZElkPUEwMjY3NjQ4V0JBVDdSVUpSQU1LJmVuY3J5cHRlZEFkSWQ9QTAxMDEzNDFFOEFKVUtJVjMxSzMmd2lkZ2V0TmFtZT1zcF9hdGYmYWN0aW9uPWNsaWNrUmVkaXJlY3QmZG9Ob3RMb2dDbGljaz10cnVl&th=1',
      'https://www.amazon.com/gp/slredirect/picassoRedirect.html/ref=pa_sp_atf_aps_sr_pg1_1?ie=UTF8&adId=A0101341E8AJUKIV31K3&url=%2FHyperX-Pulsefire-Haste-Ultra-Lightweight-Programmable%2Fdp%2FB08NSJFNSS%2Fref%3Dsr_1_2_sspa%3Fkeywords%3Dgaming%2Bmouse%26pd_rd_r%3D073adc02-9e0d-4826-a881-ef5b23617940%26pd_rd_w%3DDxPBn%26pd_rd_wg%3DjxPnR%26pf_rd_p%3D12129333-2117-4490-9c17-6d31baf0582a%26pf_rd_r%3DF535PWX760339489MFWX%26qid%3D1650030793%26sr%3D8-2-spons%26psc%3D1&qualifier=1650030793&id=6678987637669933&widgetName=sp_atf'
      ]

def getPrice(url):
    s=HTMLSession()
    r=s.get(url)
    r.html.render(sleep=1)

    product={
        'title':r.html.xpath('//*[@id="productTitle"]',first=True).text,
        'price': r.html.xpath('//*[@id="corePrice_desktop"]/div/table/tbody/tr[2]/td[2]/span[1]/span[1]', first=True).text
    }
    print(product)
    return product

mousePrice=[]
for url in urls:
    mousePrice.append(getPrice(url))

df=pd.DataFrame(mousePrice)
df.to_csv('lesson1.csv',index=False)
