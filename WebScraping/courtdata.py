import requests
from bs4 import BeautifulSoup
import pandas as pd
import json

url='https://dsjlahore.punjab.gov.pk/search?district_id=17&case_title=&case_no=&court_id=1&casecate_id=0&fir_no=&fir_year=&policestation_id='

def findSoup(url):
    r=requests.get(url)
    return BeautifulSoup(r.text,'html.parser')

caseList=[]
def getCaseList(soup):
    list=soup.find_all('tr')
    for item in list:
        try:
            td=item.find_all('td')
            case={
                'caseNo':td[0].text.strip(),
                'Title':td[1].text.strip(),
                'Category/FIR':" ".join(td[2].text.split()),
                'Judge': td[3].text.strip(),
                'Stage': td[4].text.strip(),
                'Next Hearing': td[5].text.strip(),
                'caseUrl':td[1].a['href']
            }
            caseList.append(case)
        except:
            pass

def getjudges(districtId):
    url=f'https://dsjlahore.punjab.gov.pk/getjudges/{districtId}'
    r=requests.get(url)
    jsonObj=json.loads(r.text)
    return jsonObj


def getCaseListofDistrict(districtId):
    judgesList=getjudges(districtId)
    print(judgesList)
    # for item in judgesList:
    #     judgeId=int(item)
    #     print('Getting record for Judge'+str(judgeId))
    #     url=f'https://dsjlahore.punjab.gov.pk/search?district_id={districtId}&case_title=&case_no=&court_id={judgeId}&casecate_id=0&fir_no=&fir_year=&policestation_id='
    #     soup = findSoup(url)
    #     getCaseList(soup)
    #     df = pd.DataFrame(caseList)
    #     df.to_csv(str(judgeId)+'judge.csv')

# for i in range(1,3):
#     print(f'Geting data for District {i}')
#     getCaseListofDistrict(i)
courtList=[]
def getAllCourts():
    for i in range(1, 37):
        districtCourt=getjudges(i)
        print(type(districtCourt))

        print(type(courtList))

        dictionary_copy = districtCourt.copy()
        courtList.append(dictionary_copy)
        # courtList.append(districtCourt)
    df=pd.DataFrame(courtList)
    df.to_csv('courtList.csv')

getAllCourts()




