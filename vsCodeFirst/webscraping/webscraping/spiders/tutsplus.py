from requests import request
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from webscraping.items import TutsplusItem
from scrapy.loader import ItemLoader


class TutsplusSpider(CrawlSpider):
    name = 'tutsplus'
    allowed_domains = ['tutsplus.com']
    start_urls = ['http://code.tutsplus.com/categories/']

    rules = (
        Rule(LinkExtractor(restrict_xpaths="//a[@class='alphadex__item-link']"), callback='parse_item', follow=True),
        Rule(LinkExtractor(restrict_xpaths="//a[@class='pagination__button pagination__next-button']"), callback='parse_item', follow=True),
    )

    def parse_item(self, response):
       for toturial in response.xpath("//li[@class='posts__post  ']"):
            loader=ItemLoader(item=TutsplusItem(), selector=toturial, response=response)
            loader.add_xpath('title',".//a[@class='posts__post-title ']")
            loader.add_xpath('url',".//a[@class='posts__post-title ']/@href")
            loader.add_xpath('category',"//span[@class='content-banner__title-breadcrumb-category']")
            # loader.add_value('user_agent',response.request.headers.get())
            yield loader.load_item()
            # yield{
            #     'title':toturial.xpath(".//a[@class='posts__post-title ']").get(),
            #     'url':toturial.xpath(".//a[@class='posts__post-title ']/@href").get(),
            #     'category':toturial.xpath("//span[@class='content-banner__title-breadcrumb-category']").get(),   
            # }

            # yield{
            #     'agent':response.request.headers.get()
            # }