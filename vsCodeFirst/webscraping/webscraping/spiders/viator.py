from ast import YieldFrom
import scrapy
from webscraping.items import viatorItem
from scrapy.loader import ItemLoader




class ViatorSpider(scrapy.Spider):
    name = 'viator'
    allowed_domains = ['viator.com']
    start_urls = ['https://www.viator.com/Iceland/d55-ttd/5']

    def parse(self, response):
        for item in response.xpath("//a[@class='text-dark highlight-able card-link']/@href").getall():
            yield scrapy.Request(response.urljoin(item),callback=self.parse_item)

        next_page=response.xpath("//a[@aria-label='Next']/@href").extract_first()
        if next_page is not None:
            next_page_link=response.urljoin(next_page)
            yield scrapy.Request(url=next_page_link,callback=self.parse)

    def parse_item(self, response):
        loader=ItemLoader(item=viatorItem(),response=response)

        loader.add_xpath('title',"//h1[@class='title__1Wwg title2__C3R7']")
        loader.add_xpath('price',"//span[@class='moneyView__2HPx']")
        loader.add_xpath('review',"//div[@class='ratingReviews__2CyF']")
        loader.add_xpath('overview',"//div[@class='overviewWrapper__bMs4']/div/div")
        loader.add_xpath('included',"//ul[@class='featureList__34Y_ noBullet__2pPd']/li[@class='feature__1-FD']")
        loader.add_value('url',response.url)               

        yield loader.load_item()
        #loader.add_xpath('review',".//p[@class='boost-pfs-filter-product-item-price']/span")
        #loader.add_xpath('phoneNumber',".//div[@class='boost-pfs-filter-product-item-image']/a/@href")
        #yield{'title':response.xpath("//h1[@class='title__1Wwg title2__C3R7']").get()}
#price
#//span[@class='moneyView__2HPx']

#overview
#//div[@class='overviewWrapper__bMs4']/div/div
#included
#//ul[@class='featureList__34Y_ noBullet__2pPd']/li[@class='feature__1-FD']

#link
#//div[@class='table-header']/a
#title
#//div[@class='table-header']/a/h1