import scrapy


class AmazonSpider(scrapy.Spider):
    name = 'amazon'
    allowed_domains = ['amazom.com']
    start_urls = ['https://www.amazon.sg/s?bbn=10286233051&rh=n%3A10286233051%2Cp_n_publication_date%3A6439959051&dc&qid=1655158611&rnid=6439944051&ref=lp_10286233051_nr_p_n_publication_date_0']

    def parse(self, response):
        for book in response.xpath("//div[@class='sg-col-inner']/div[@class='a-section a-spacing-small a-spacing-top-small']").getall():
            yield{
            'title':book.xpath(".//span[@class='a-size-medium a-color-base a-text-normal']").get()
            }
        
