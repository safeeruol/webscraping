import selectors
from numpy import absolute
import scrapy
from webscraping.items import BookItem
from scrapy.loader import ItemLoader

class BookimagesSpider(scrapy.Spider):
    name = 'bookimages'
    def __init__(self, *a, **kw):
        self.allowed_domains = ['http://books.toscrape.com']
        self.start_urls = ['http://books.toscrape.com/']
        
    def parse(self, response):
        for book in response.xpath("//article[@class='product_pod']"):
            relative_path=book.xpath(".//div[@class='image_container']/a/img/@src").extract_first()
            absolute_path=response.urljoin(relative_path)
            yield{
                "book_title": book.xpath(".//h3/a/@title").extract_first(),
                # "images_urls": book.xpath(".//div[@class='image_container']/a/img/@src").extract_first()
                "image_urls": absolute_path

            }
            # loader=ItemLoader(item=BookItem(), selector=book, response=response)
           
            # loader.add_value('images',absolute_path)
            # loader.add_xpath('book_title',".//h3/a/@title")

            # loader.load_item()
