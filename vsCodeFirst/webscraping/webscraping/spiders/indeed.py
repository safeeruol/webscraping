from ast import YieldFrom
from urllib.request import Request
import scrapy


class IndeedSpider(scrapy.Spider):
    name = 'indeed'
    allowed_domains = ['pk.indeed.com']
    start_urls = ['http://pk.indeed.com/jobs?q=React&start=50']

    def parse(self, response):
        for job in response.xpath('//div[@class="job_seen_beacon"]'):
            # title=job.xpath(".//span[starts-with(@id,'jobTitle-')]/text()").get()
            companyName=job.xpath('.//span[@class="companyName"]/text()').get()
            companyLocation=job.xpath(".//div[@class='companyLocation']/text()").get()
            salary=job.xpath('.//div[@class="attribute_snippet"]/text()').get()

            
            # des_list=[]
            # for description in job.xpath(".//div[@class='job-snippet']/ul/li").getall():
            #     des_list.append(description)

            yield {
                # 'title':title,
                'companyName':companyName,
                'companyLocation':companyLocation,
                'salary':salary,
                'url':response.url
            }

        next_page=response.xpath("//a[@aria-label='Next']/@href").get()
        if next_page is not None:
            yield scrapy.Request(url=response.urljoin(next_page),callback=self.parse)

           