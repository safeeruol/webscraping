from requests import request
import scrapy
from scrapy.loader import ItemLoader
from webscraping.items import FightItem
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor


class BestfightoddsSpider(CrawlSpider):
    name = 'fight'
    allowed_domains = ['bestfightodds.com']
    # rules = (
    #     Rule(LinkExtractor(allow=r"events/"), callback='parse_fighter', follow=True),
    # )
    start_urls = [
                    'https://www.bestfightodds.com/events/ufc-fight-night-kattar-vs-emmett-2523'
                    ]
                    

    # def parse(self, response):
    #     for item in response.xpath("//div[@class='table-header']/a/@href").getall():
    #         yield scrapy.Request(response.urljoin(item),callback=self.parse_fighter)
    #         #yield {'urls':response.urljoin(item)}

    def parse(self, response):
        for item in response.xpath("//div[@class='table-div']/div[2]/div[3]/table/tbody/tr/th[@scope='row']/a[not(contains(@class, 'pr'))]"):
            loader=ItemLoader(item=FightItem(), selector=item, response=response)
            
            loader.add_value("url",response.url)
            # loader.add_xpath("url",response.urljoin(".//@href"))
            loader.add_xpath("fighter",".//span")
            loader.add_xpath("draftKing",".//parent::td[1]/span[1]")
            # loader.add_xpath("fighter",".//th/a/span")
            yield loader.load_item()
            #yield{"url": item.xpath(".//th/a/text()").get()}