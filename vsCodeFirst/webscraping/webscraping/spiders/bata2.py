from scrapy.spiders import SitemapSpider  
import scrapy
from scrapy.loader import ItemLoader
from webscraping.items import WebscrapingItem, ItemDetails

class Bata2Spider(scrapy.Spider):
    name = 'bata2'
    allowed_domains = ['bata.com.pk']
     
    def start_requests(self):
        yield scrapy.Request('https://www.bata.com.pk/collections/women-1', self.parse_url)
        yield scrapy.Request('https://www.bata.com.pk/collections/justice-league', self.parse_url)
        yield scrapy.Request('https://www.bata.com.pk/collections/prive', self.parse_url)
        yield scrapy.Request('https://www.bata.com.pk/collections/bata-3d-energy-women', self.parse_url)
        yield scrapy.Request('https://www.bata.com.pk/collections/leena', self.parse_url)
        yield scrapy.Request('https://www.bata.com.pk/collections/bata-women', self.parse_url)
        yield scrapy.Request('https://www.bata.com.pk/collections/men-wallets', self.parse_url)
       

    def parse_url(self, response):
        for item in response.xpath("//div[@class='boost-pfs-filter-product-item-inner']"):
            url=item.xpath(".//div[@class='boost-pfs-filter-product-item-image']/a/@href").get()
            yield scrapy.Request(response.urljoin(url), self.parse_item_details)

    def parse_item_details(self, response):
             
        item=response.xpath("//div[@id='content']")
        loader=ItemLoader(item=ItemDetails(),selector=item,response=response)
        loader.add_xpath('name',".//h1[@itemprop='name']")                
        loader.add_xpath('name',".//h1[@itemprop='name']")                
        yield loader.load_item()

