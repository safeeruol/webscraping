from scrapy.spiders import SitemapSpider  
import scrapy
from scrapy.loader import ItemLoader
from webscraping.items import WebscrapingItem

class BataSpider(scrapy.Spider):
    name = 'bata'
    allowed_domains = ['bata.com.pk']
    start_urls = ['https://www.bata.com.pk/collections/women-1']

    def parse(self, response):
        for item in response.xpath("//div[@class='boost-pfs-filter-product-item-inner']"):

            loader=ItemLoader(item=WebscrapingItem(),selector=item,response=response)

            loader.add_xpath('name',".//a[@class='boost-pfs-filter-product-item-title']")
            loader.add_xpath('price',".//p[@class='boost-pfs-filter-product-item-price']/span")
            loader.add_xpath('url',".//div[@class='boost-pfs-filter-product-item-image']/a/@href")

            yield loader.load_item()

        

