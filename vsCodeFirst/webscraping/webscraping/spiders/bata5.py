import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from scrapy.loader import ItemLoader
from webscraping.items import WebscrapingItem, ItemDetails


class Bata5Spider(CrawlSpider):
    name = 'bata5'
    allowed_domains = ['bata.com.pk']
    start_urls = ['http://bata.com.pk/']

    rules = (
        # Rule(LinkExtractor(allow=r'products/'), callback='parse_item', follow=True),
        Rule(LinkExtractor(allow=r'collections/'), callback='parse_item', follow=True),
    )

    def parse_item(self, response):
        sizes=[]
        unavalabe_sizes=[]

        for size in response.xpath(".//div[@class='swatch-element ']"):
            sizes.append(size.xpath(".//label/text()").get())
        for unavalable in response.xpath(".//div[@class='swatch-element soldout']"):
            unavalabe_sizes.append(unavalable.xpath(".//label/text()").get())

        item=response.xpath("//div[@id='content']")
        loader=ItemLoader(item=ItemDetails(),selector=item,response=response)

        loader.add_xpath('name',".//h1[@itemprop='name']")                
        loader.add_xpath('price',"//div[@id='product-price']/span/span[@class='money']")                
        loader.add_xpath('productCode',"//div[@class='custom_pro_description']/p") 
        loader.add_xpath('color',"//div[@class='swatch-element color ']/div") 
        loader.add_value('sizes',sizes)             
        loader.add_value('unavalabe_sizes',unavalabe_sizes)             
        loader.add_value('url',response.url)               
                
        yield loader.load_item()
        # yield{
        #     'url':response.url
        # }