import scrapy
from scrapy.loader import ItemLoader
from webscraping.items import WebscrapingItem, ItemDetails


class Bata3Spider(scrapy.Spider):
    name = 'bata3'
    allowed_domains = ['bata.com.pk']
     
    start_urls=[
        'https://www.bata.com.pk/collections/women-1',
        'https://www.bata.com.pk/collections/justice-league',
        'https://www.bata.com.pk/collections/prive',
        'https://www.bata.com.pk/collections/bata-3d-energy-women',
        'https://www.bata.com.pk/collections/leena',
        'https://www.bata.com.pk/collections/bata-women',
        'https://www.bata.com.pk/collections/men-wallets',
    ]

    def parse(self, response):
        for item in response.xpath("//div[@class='boost-pfs-filter-product-item-inner']"):
            url=item.xpath(".//div[@class='boost-pfs-filter-product-item-image']/a/@href").get()
            yield scrapy.Request(response.urljoin(url), self.parse_item_details)

    def parse_item_details(self, response):
             
        item=response.xpath("//div[@id='content']")
        loader=ItemLoader(item=ItemDetails(),selector=item,response=response)
        loader.add_xpath('name',".//h1[@itemprop='name']")                
        yield loader.load_item()

