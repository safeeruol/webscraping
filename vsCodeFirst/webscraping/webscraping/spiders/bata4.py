import scrapy
from scrapy.loader import ItemLoader
from webscraping.items import WebscrapingItem, ItemDetails


class Bata4Spider(scrapy.Spider):
    name = 'bata4'
    allowed_domains = ['bata.com.pk']
     
    start_urls=[
        'https://www.bata.com.pk/collections/women-1',
        'https://www.bata.com.pk/collections/justice-league',
        'https://www.bata.com.pk/collections/prive',
        'https://www.bata.com.pk/collections/bata-3d-energy-women',
        'https://www.bata.com.pk/collections/leena',
        'https://www.bata.com.pk/collections/bata-women',
        'https://www.bata.com.pk/collections/men-wallets'
    ]

    def parse(self, response):
        for item in response.xpath("//div[@class='boost-pfs-filter-product-item-inner']"):
            url=item.xpath(".//div[@class='boost-pfs-filter-product-item-image']/a/@href").get()
            yield scrapy.Request(response.urljoin(url), self.parse_item_details)

    def parse_item_details(self, response):
        sizes=[]
        unavalabe_sizes=[]

        for size in response.xpath(".//div[@class='swatch-element ']"):
            sizes.append(size.xpath(".//label/text()").get())
        for unavalable in response.xpath(".//div[@class='swatch-element soldout']"):
            unavalabe_sizes.append(unavalable.xpath(".//label/text()").get())

        item=response.xpath("//div[@id='content']")
        loader=ItemLoader(item=ItemDetails(),selector=item,response=response)

        loader.add_xpath('name',".//h1[@itemprop='name']")                
        loader.add_xpath('price',"//div[@id='product-price']/span/span[@class='money']")                
        loader.add_xpath('productCode',"//div[@class='custom_pro_description']/p") 
        loader.add_xpath('color',"//div[@class='swatch-element color ']/div") 
        loader.add_value('sizes',sizes)             
        loader.add_value('unavalabe_sizes',unavalabe_sizes)             
        loader.add_value('url',response.url)               
                
        yield loader.load_item()