# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

from webbrowser import get
import scipy
import scrapy
from scrapy.loader.processors import MapCompose, TakeFirst, Join
from w3lib.html import remove_tags

def remove_quotations(value):
    return value.replace(u"\u201d", '').replace(u"\u201c", '')

def strip_value(value):
    return value.strip()

def remove_review(value):
    return value.replace(u"Reviews", '')

class AmazonBookItem(scrapy.Item):
    title=scrapy.Field(
        input_processor=MapCompose(remove_tags),
        output_processor=TakeFirst()
    )
    author=scrapy.Field(
        input_processor=MapCompose(remove_tags),
        output_processor=TakeFirst()
    )
    price=scrapy.Field(
        input_processor=MapCompose(remove_tags),
        output_processor=TakeFirst()
    )
    image_url=scrapy.Field(
        input_processor=MapCompose(remove_tags),
        output_processor=TakeFirst()
    )

class WebscrapingItem(scrapy.Item):
    name=scrapy.Field(
        input_processor=MapCompose(remove_tags),
        output_processor=TakeFirst()
    )
    price=scrapy.Field(
        input_processor=MapCompose(remove_tags),
        output_processor=TakeFirst()
    )
    url=scrapy.Field(
        input_processor=MapCompose(remove_tags),
        output_processor=TakeFirst()
    )

class TutsplusItem(scrapy.Item):
    title=scrapy.Field(
        input_processor=MapCompose(remove_tags,strip_value),
        output_processor=TakeFirst()
    )
    url=scrapy.Field(
        input_processor=MapCompose(remove_tags,strip_value),
        output_processor=TakeFirst()
    )
    category=scrapy.Field(
        input_processor=MapCompose(remove_tags,strip_value),
        output_processor=TakeFirst()
    )
    user_agent=scrapy.Field(
        input_processor=MapCompose(),
        output_processor=TakeFirst()
    )


class ItemDetails(scrapy.Item):
    name=scrapy.Field(
        input_processor=MapCompose(remove_tags,strip_value),
        output_processor=TakeFirst()
    )
    price=scrapy.Field(
        input_processor=MapCompose(remove_tags,strip_value),
        output_processor=TakeFirst()
    )
    productCode=scrapy.Field(
        input_processor=MapCompose(remove_tags,strip_value),
        output_processor=TakeFirst()
    )
    url=scrapy.Field(
        input_processor=MapCompose(strip_value),
        output_processor=TakeFirst()
    )
    sizes=scrapy.Field(
        input_processor=MapCompose(remove_tags,strip_value),
    )
    unavalabe_sizes=scrapy.Field(
        input_processor=MapCompose(remove_tags,strip_value),
    )
    color=scrapy.Field(
        input_processor=MapCompose(remove_tags,strip_value),
        output_processor=TakeFirst()
    )
    thumbnails=scrapy.Field(
        input_processor=MapCompose(remove_tags,strip_value),
    )

class viatorItem(scrapy.Item):
    title=scrapy.Field(
        input_processor=MapCompose(remove_tags,strip_value,remove_quotations),
        output_processor=TakeFirst()
    )
    review=scrapy.Field(
        input_processor=MapCompose(remove_tags),
        output_processor=TakeFirst()
    )
    phoneNumber=scrapy.Field(
        input_processor=MapCompose(remove_tags,strip_value),
        output_processor=TakeFirst()
    )
    price=scrapy.Field(
        input_processor=MapCompose(remove_tags,strip_value),
        output_processor=TakeFirst()
    )
    overview=scrapy.Field(
        input_processor=MapCompose(remove_tags,strip_value,remove_review),
        output_processor=TakeFirst()
    )
    included=scrapy.Field(
        input_processor=MapCompose(remove_tags,strip_value),
    )
    departure=scrapy.Field(
        input_processor=MapCompose(remove_tags,strip_value),
        output_processor=TakeFirst()
    )
    expact=scrapy.Field(
        input_processor=MapCompose(remove_tags,strip_value),
        output_processor=TakeFirst()
    )
    aditionalInfo=scrapy.Field(
        input_processor=MapCompose(remove_tags,strip_value),
        output_processor=TakeFirst()
    )
    cancelPolicy=scrapy.Field(
        input_processor=MapCompose(remove_tags,strip_value),
        output_processor=TakeFirst()
    )
    url=scrapy.Field(
        input_processor=MapCompose(strip_value),
        output_processor=TakeFirst()
    )

class FightItem(scrapy.Item):
    fighter=scrapy.Field(
        input_processor=MapCompose(remove_tags,strip_value),
        output_processor=TakeFirst()
    )
    url=scrapy.Field(
        input_processor=MapCompose(strip_value),
        output_processor=TakeFirst()
    )
    draftKing=scrapy.Field(
        input_processor=MapCompose(strip_value,remove_tags),
        output_processor=TakeFirst()
    )
  
class BookItem(scrapy.Item):
    images=scrapy.Field()
    book_title=scrapy.Field(
        input_processor=MapCompose(strip_value),
        output_processor=TakeFirst()
    )