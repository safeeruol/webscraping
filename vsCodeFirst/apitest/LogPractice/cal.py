import logging
import os
if not os.path.exists("Logs"):
    os.makedirs("Logs")
logging.basicConfig(filename='Logs/cal.log',level=logging.DEBUG,format='%(asctime)s:%(filename)s:%(funcName)s:%(levelname)s:%(levelno)s:%(lineno)d:%(message)s')

def add(x,y):
    return x+y

def subtract(x,y):
    return x-y

def multiply(x,y):
    return x*y

def divide(x,y):
    return x/y

num1=10
num2=5

add_result=add(num1,num2)
logging.debug('Add: {}+{}={}'.format(num1,num2,add_result))

subtract_result=subtract(num1,num2)
logging.info('Sub: {}+{}={}'.format(num1,num2,subtract_result))

multiply_result=multiply(num1,num2)
logging.warning('Mul: {}+{}={}'.format(num1,num2,multiply_result))

divide_result=divide(num1,num2)
logging.error('Div: {}+{}={}'.format(num1,num2,divide_result))
