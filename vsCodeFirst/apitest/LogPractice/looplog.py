import datetime
import logging
import schedule
import time
from logging.handlers import TimedRotatingFileHandler
import os


if not os.path.exists("Logs"):
    os.makedirs("Logs")

#logging.basicConfig(filename='Logs/looping.log',level=logging.DEBUG,format='%(asctime)s:%(filename)s:%(funcName)s:%(levelname)s:%(levelno)s:%(lineno)d:%(message)s')
formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
handler = TimedRotatingFileHandler('Logs/flask_server.log', when="S", interval=30, delay=True,encoding='utf8')
handler.suffix = "%Y-%m-%d_%H-%M-%S"
handler.setFormatter(formatter)
logger = logging.getLogger()
logger.setLevel(logging.INFO)
logger.addHandler(handler)

try:
    print("started")
    while 1:
        #schedule.run_pending()
        time.sleep(0.5)
        logging.info("Pakistan")
except Exception:
    logging.debug("CRON was unble to start. Something Wrong in StartCron function.")
    logging.exception("CRON was unable to start. Something Wrong in StartCron function.", exc_info=True)


