import datetime
import logging
import schedule
import time
from logging.handlers import RotatingFileHandler
class Workflow:    

    def setupLoggingToFile():
        logFilePath = 'Logs/sbcf.log'
        logdate = datetime.datetime.now().strftime('%Y-%m-%d')
        logging.basicConfig(
            format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
            datefmt='%m-%d-%y %H:%M:%S',
            level=logging.DEBUG,
            handlers=[RotatingFileHandler(logFilePath + "logfile_"+logdate+".log",maxBytes=10485760, backupCount=100)])   

    def StartWorkflow(self):
        try:
            print("New Cron Cycle Started..")
            logging.info("Something went wrong.", exc_info=True)

        except Exception:
            logging.exception("Something went wrong.", exc_info=True)

    def StartCron(self):
        print("Started..")
        
        try:
            schedule.every(5).seconds.do(self.StartWorkflow)
            while 1:
                schedule.run_pending()
                time.sleep(1)
            logging.info("Something went wrong.", exc_info=True)

        except Exception:
            logging.debug("CRON was unable to start. Something Wrong in StartCron function.")
            logging.exception("CRON was unable to start. Something Wrong in StartCron function.", exc_info=True)


A = Workflow()
A.StartCron()
