import scrapy
from scrapy.spiders import CrawlSpider,Rule
from scrapy.linkextractors import LinkExtractor


class ChocoSpider(CrawlSpider):
    name = 'choco'
    allowed_domains = ['chocolate.co.uk']
    start_urls = ['http://chocolate.co.uk/']

#https://www.chocolate.co.uk/products/box-of-the-month-subscription-1
#https: // www.chocolate.co.uk / collections / luxury - chocolate - bars
    rules = (
        Rule(LinkExtractor(allow='collections')),
        Rule(LinkExtractor(allow='products', deny='collections'), callback='parse_item'),
    )

    def parse(self, response):
        pass

    def parse_item(self, response):
        yield {
            'ProductURL':response.request.url,
            'ProdcutTitle':response.xpath("//h1/text()").get(),
            'ProductPrice':response.xpath("//span[contains(@class,'price--large')]/text()[2]").get(),
            'DiscountPrice':response.xpath("//span[contains(@class,'price--compare')]/text()[2]").get(),
            'Description':response.xpath("//div[contains(@class,'product-form__description')]/p/text()").get(),
            'ProductWeight':response.xpath("//p[contains(text(),'Weight')]/text()").get().split(':')[2].strip(),
            'ProductImageUrl':response.xpath("//div[contains(@class,'product__media-image-wrapper')]/img/@srcset").get().split(',')[1].split(' ')[1]
        }

