import scrapy
from scrapy.crawler import CrawlerProcess
from scrapy.shell import inspect_response
class FurnitureSpider(scrapy.Spider):
    name = 'furniture'
    allowed_domains = ['wayfair.com']
    start_urls = ['https://www.wayfair.com/furniture/sb0/sectionals-c413893.html']

    def parse(self, response):
        list= response.xpath("//a[@class='_1yxeg5wb_685']")
        for item in list:
            # yield {
            #     'url':item.xpath(".//@href").get()
            # }

            yield scrapy.Request(
                url=item.xpath(".//@href").get(),
                callback=self.parse_details
            )

    def parse_details(self,response):
        yield {
            'title':response.xpath("//h1[contains(@class,'pl-Heading--pageTitle')]/text()").get(),
            'price':response.xpath("//span[@class='pl-Box--display-inline-block pl-Box--mr-1 pl-Box--pr-1 pl-Price-V2 pl-Price-V2--5000 pl-Box--saleColor']/text()").get(),
            'url':response.url
        }





        # inspect_response(response, self)

        # print('pageFrom'+self.pageFrom )
        # print('pageTo'+self.pageTo )
        #
        # next_page = response.xpath("//a[@data-enzyme-id='paginationNextPageLink']/@href").get()
        # if next_page is not None:
        #
        #     print(next_page)
        #     yield scrapy.Request(next_page, callback=self.parse)

        #product variations: like color, size.....
        #linkproduct,
        # producttitle,
        # sku,
        # price,
        # description,
        # Weights & Dimensions,
        # Specifications,
        # product catalogy,
        # shortlink,

# process = CrawlerProcess()
# process.crawl(FurnitureSpider,input='inputargument',pageFrom='1',pageTo='10')
# process.start()

#
# from scrapy import cmdline
#
# cmdline.execute("scrapy crawl furniture -a pageFrom='1' -a pageTo='5' -o output.json".split())

#process.crawl(spider, input='inputargument', first='James', last='Bond')