import scrapy


class FakePlantSpider(scrapy.Spider):
    name = 'fake-plant'
    allowed_domains = ['fake-plant.co.uk']
    start_urls = ['http://fake-plant.co.uk/']

    def parse(self, response):
        pass
