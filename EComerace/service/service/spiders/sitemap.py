
from datetime import datetime
from scrapy.spiders import SitemapSpider

class FilteredSitemapSpider(SitemapSpider):
    name = 'sitemap_spider'
    allowed_domains = ['www.servis.pk']
    sitemap_urls = ['https://www.servis.pk/sitemap_products_1.xml?from=2617304088694&to=6802312790134']

    def sitemap_filter(self, entries):
        for entry in entries:
            yield entry