import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor


class FakePlantSpider(CrawlSpider):
    name = 'fake_plant'
    allowed_domains = ['www.fake-plants.co.uk']
    start_urls = ['https://www.fake-plants.co.uk/']
    #https://www.fake-plants.co.uk/product-category/fake-yucca-plants/
    #https://www.fake-plants.co.uk/product/closer-to-nature-artificial-2ft-1-yucca-plant-artificial-silk-plant-and-tree-range/
    rules=(
        Rule(LinkExtractor(allow='product-category')),
        Rule(LinkExtractor(allow='product',deny='product-category'),callback='parse_item'),
    )

    def parse(self, response):
        pass

    def parse_item(self, response):
        yield {
            'ProductURL': response.request.url,
            'ProductTitle':response.xpath("//h1[@class='product_title entry-title']/text()").get(),
            'Category':response.xpath("//span[@class='posted_in']/a[@rel='tag']/text()").get(),
            'Tages':response.xpath("//span[@class='tagged_as']/a/text()").getall(),
            'ImageURL':response.xpath("//a[@itemprop='image']/@href").get()
        }
