import scrapy
from scrapy.spiders import CrawlSpider ,Rule
from scrapy.linkextractors import LinkExtractor

class ProductoneSpider(CrawlSpider):
    name = 'productone'
    allowed_domains = ['sipwhiskey.com']
    start_urls = ['http://sipwhiskey.com/']

    rules=(
        Rule(LinkExtractor(allow='collections/bourbon', deny='products')),
        Rule(LinkExtractor(allow='products'), callback='parse_item')
    )

    def parse(self, response):
        pass

    def parse_item(self, response):
        yield {
            'title':response.xpath("//h1[@class='title']/text()").get(),
            'price':response.xpath("//span[@class='price theme-money']/text()").get(),
            'number_reviews':response.xpath("//span[@class='stamped-badge-caption']/@aria-label").get(),
            'prodcut_url': response.request.url
        }
