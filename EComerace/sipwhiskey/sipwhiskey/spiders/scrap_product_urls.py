import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor


class ScrapProductUrlsSpider(CrawlSpider):
    name = 'scrap_product_urls'
    allowed_domains = ['sipwhiskey.com']
    start_urls = ['http://sipwhiskey.com/']
    # https://sipwhiskey.com/collections/custom-engraving
    # https://sipwhiskey.com/collections/custom-engraving/products/brothers-bond-straight-bourbon-whiskey
    rules=(
        Rule(LinkExtractor(allow='products'),callback='parse'),
    )

    def parse(self, response):
        yield {
            'prodcut_url':response.request.url
        }
