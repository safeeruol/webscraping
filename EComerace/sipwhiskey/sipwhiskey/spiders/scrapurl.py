import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor


class ScrapurlSpider(CrawlSpider):
    name = 'scrapurl'
    allowed_domains = ['sipwhiskey.com']
    start_urls = ['http://sipwhiskey.com/']

    rules=(
        Rule(LinkExtractor(allow='collections', deny='products'),callback='parse_url'),
    )

    #https://sipwhiskey.com/collections/custom-engraving
    #https://sipwhiskey.com/collections/custom-engraving/products/brothers-bond-straight-bourbon-whiskey

    def parse(self, response):
        pass

    def parse_url(self, response):
        yield {
            'url':response.request.url
        }
