import imp
from nturl2path import url2pathname
import scrapy
from ..utils import URL,cookie_parser
import json
from scrapy.loader import ItemLoader
from ..items import ZillowItem

class ZillowHousesSpider(scrapy.Spider):
    name = 'zillow_houses'
    allowed_domains = ['www.zillow.com']
    start_urls = [URL]

    def start_requests(self):
        yield scrapy.Request(
            url=URL,
            callback=self.parse,
            cookies=cookie_parser(),
            meta={
                'currentPage': 1
            }
        )

    def parse(self, response):
        json_res= json.loads(response.body)
        houses=json_res.get('cat1').get('searchResults').get('listResults')
        for house in houses:
            # print(house.get('zpid'))
            loader = ItemLoader(item=ZillowItem())
            loader.add_value('id',house.get('id'))
            loader.add_value('address',house.get('address'))
            loader.add_value('addressCity',house.get('addressCity'))
            loader.add_value('addressState',house.get('addressState'))
            loader.add_value('addressStreet',house.get('addressStreet'))
            loader.add_value('addressZipcode',house.get('addressZipcode'))
            loader.add_value('image_urls',house.get('imgSrc'))
            loader.add_value('latitude',house.get('latLong').get('latitude'))
            loader.add_value('longitude',house.get('latLong').get('longitude'))
            loader.add_value('statusText',house.get('statusText'))
            loader.add_value('statusType',house.get('statusType'))
            loader.add_value('beds',house.get('beds'))
            loader.add_value('baths',house.get('baths'))
            loader.add_value('price',house.get('price'))


            yield loader.load_item()

        total_pages=json_res.get('cat1').get('searchList').get('totalPages')

            
        


