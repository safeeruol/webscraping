from http.cookies import SimpleCookie
from urllib.parse import parse_qs, urlencode, urlparse
import json

URL='https://www.zillow.com/search/GetSearchPageState.htm?searchQueryState=%7B%22mapBounds%22%3A%7B%22north%22%3A40.678169094932734%2C%22east%22%3A-111.77257809448243%2C%22south%22%3A40.56950638360492%2C%22west%22%3A-111.97342190551758%7D%2C%22mapZoom%22%3A13%2C%22isMapVisible%22%3Afalse%2C%22filterState%22%3A%7B%22isForSaleForeclosure%22%3A%7B%22value%22%3Afalse%7D%2C%22isAllHomes%22%3A%7B%22value%22%3Atrue%7D%2C%22isAuction%22%3A%7B%22value%22%3Afalse%7D%2C%22isNewConstruction%22%3A%7B%22value%22%3Afalse%7D%2C%22isForRent%22%3A%7B%22value%22%3Atrue%7D%2C%22isForSaleByOwner%22%3A%7B%22value%22%3Afalse%7D%2C%22isComingSoon%22%3A%7B%22value%22%3Afalse%7D%2C%22isForSaleByAgent%22%3A%7B%22value%22%3Afalse%7D%7D%2C%22isListVisible%22%3Atrue%2C%22pagination%22%3A%7B%22currentPage%22%3A2%7D%7D&wants={%22cat1%22:[%22listResults%22]}&requestId=8'


def cookie_parser():
    cookie_string='zguid=24|%24d6b9491f-80f4-48e5-92fd-e503ca996f55; _ga=GA1.2.248199339.1656358615; zjs_user_id=null; zg_anonymous_id=%223bf0315e-9d72-4bdc-a88b-ba6c8e5f88b3%22; zjs_anonymous_id=%22d6b9491f-80f4-48e5-92fd-e503ca996f55%22; _pxvid=5e005324-f648-11ec-9ce1-427667466859; _gcl_au=1.1.2001210952.1656358695; __pdst=78d938c43a9f44609410246cd1520a9c; _cs_c=0; _fbp=fb.1.1656358700659.1300817746; _pin_unauth=dWlkPVptUmhPR0kwTW1NdE4yVXpOUzAwTXpjNUxUZ3dOVE10WkdGaFlqbGpOV05pTjJVMw; _clck=k96ohz|1|f32|0; __gads=ID=e26c019942c5d4ee:T=1657495879:S=ALNI_MaCAr_iCNUOGjkwK9UjD6s9GkjzJQ; zgsession=1|b33cfd8b-474f-4fe3-893b-467e845a47a0; _gid=GA1.2.702034496.1657555451; pxcts=d1ee18df-012a-11ed-b4e5-6a7161524941; KruxPixel=true; DoubleClickSession=true; _hp2_id.1215457233=%7B%22userId%22%3A%221711602135825172%22%2C%22pageviewId%22%3A%221234422967890221%22%2C%22sessionId%22%3A%228554914151248443%22%2C%22identity%22%3A%22d6b9491f80f448e592fde503ca996f55%22%2C%22trackerVersion%22%3A%224.0%22%2C%22identityField%22%3Anull%2C%22isIdentified%22%3A1%7D; _hp2_ses_props.1215457233=%7B%22r%22%3A%22https%3A%2F%2Fwww.google.com%2F%22%2C%22ts%22%3A1657555462071%2C%22d%22%3A%22www.zillow.com%22%2C%22h%22%3A%22%2F%22%7D; _cs_id=c67ecac4-6402-a59d-886c-399b8eba43f2.1657555464.1.1657555464.1657555464.1.1691719464378; utag_main=v_id:0181eaaa22ea0001c53454e4ae730506f032706700bd0$_sn:2$_se:1$_ss:1$_st:1657557260599$ses_id:1657555460599%3Bexp-session$_pn:1%3Bexp-session$dcsyncran:1%3Bexp-session$tdsyncran:1%3Bexp-session$dc_visit:1$dc_event:1%3Bexp-session$dc_region:eu-central-1%3Bexp-session$ttd_uuid:6040cbd2-e854-46af-b280-b68e3de5d1ac%3Bexp-session; _cs_s=1.5.0.1657557266810; JSESSIONID=713A4654E5C79286393B91092EC18FE7; _px3=97dadfa2fdd55f83929c6c9f7135d890573b23c9cd555144cdd54ae3868134f7:YPB0K26pHLqslhyrmdggOdoO21pUEA6j5EeyQlJ4r4hCq9MZU+8/mIzAM8q+aHy0KSdo9XjrM+5HA6BzeD6eiQ==:1000:oI621RSvgmFV+9AMWYGD75pAXvhyCCLlFqWmI6r1Ynw9wuLZSjHlNYvefZAn2380jloUVTGMsOQVVDfyIoxWCSG2sNl1qSrgoVMH6Z4mTHVlBkiWabEpSJxJ61fjlGqagvzxasJmu8Hf5FUrrxCG6I6V2fUSUPteXEXoo12DCvZocc7UGq+3V1IvD/D5AGFvNL5hPcpvpb/4Ab4KzRBi/A==; _uetsid=1f93d5e0013311eda64617546e8b444e; _uetvid=1f941fc0013311edae0161ee8075c270; __gpi=UID=00000792fd3177e8:T=1657495879:RT=1657551950:S=ALNI_MZTRy27Mo_r_RMXMtPiEtVETPvw3w; _gat=1; KruxAddition=true; AWSALB=1FXJbQMny8/xJ5yuwv3CRs5DS6aLOwYK9Cngw7VYm5mID+ghOIEnO/AwLnqrJIFtCA+G5A0z0BToxklCC1QpiAgmlw4+n/JIJi385CshRXLpsDroAoj0A/5qDWl5; AWSALBCORS=1FXJbQMny8/xJ5yuwv3CRs5DS6aLOwYK9Cngw7VYm5mID+ghOIEnO/AwLnqrJIFtCA+G5A0z0BToxklCC1QpiAgmlw4+n/JIJi385CshRXLpsDroAoj0A/5qDWl5; search=6|1660143971896%7Crect%3D40.77958922129227%252C-111.63056422565637%252C40.34453904983384%252C-112.433939469797%26rid%3D240152%26disp%3Dmap%26mdm%3Dauto%26p%3D1%26z%3D1%26fs%3D0%26fr%3D1%26mmm%3D0%26rs%3D0%26ah%3D0%26singlestory%3D0%26housing-connector%3D0%26abo%3D0%26garage%3D0%26pool%3D0%26ac%3D0%26waterfront%3D0%26finished%3D0%26unfinished%3D0%26cityview%3D0%26mountainview%3D0%26parkview%3D0%26waterview%3D0%26hoadata%3D1%26zillow-owned%3D0%263dhome%3D0%26featuredMultiFamilyBuilding%3D0%26excludeNullAvailabilityDates%3D0%09%09240152%09%09%09%09%09%09; _clsk=yxk2vs|1657555534405|3|0|l.clarity.ms/collect'
    cookie= SimpleCookie()
    cookie.load(cookie_string)

    cookies={}
    for key , morsel in cookie.items():
        cookies[key]= morsel.value

    return cookies

def parse_new_url(url, page_number):
    url_parsed=urlparse(url)
    query_string=parse_qs(url_parsed.query)
    search_query_state=json.loads(query_string.get('searchQueryState')[0])
    search_query_state['pagination']={'currentPage':page_number}
    query_string.get('searchQueryState')[0] = search_query_state
    encoded_qs = urlencode(query_string, doseq=1)
    new_url = f"https://www.zillow.com/search/GetSearchPageState.htm?{encoded_qs}"
    return new_url

    
