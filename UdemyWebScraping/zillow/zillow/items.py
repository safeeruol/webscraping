# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import imp
import scrapy
from scrapy.loader.processors import TakeFirst

class ZillowItem(scrapy.Item):
    id=scrapy.Field(
        output_processor=TakeFirst()
    )
    address=scrapy.Field(
        output_processor=TakeFirst()
    )
    addressCity=scrapy.Field(
        output_processor=TakeFirst()
        )
    addressState=scrapy.Field(
        output_processor=TakeFirst()
    )
    addressStreet=scrapy.Field(
        output_processor=TakeFirst()
    )
    addressZipcode=scrapy.Field(
        output_processor=TakeFirst()
    )
    image_urls = scrapy.Field()
    images = scrapy.Field()
    
    latitude=scrapy.Field(
        output_processor=TakeFirst()
    )
    longitude=scrapy.Field(
        output_processor=TakeFirst()
    )
    statusText=scrapy.Field(
        output_processor=TakeFirst()
    )
    statusType=scrapy.Field(
        output_processor=TakeFirst()
    )
    units=scrapy.Field(
        output_processor=TakeFirst()
    )
    beds=scrapy.Field(
        output_processor=TakeFirst()
    )
    baths=scrapy.Field(
        output_processor=TakeFirst()
    )
    price=scrapy.Field(
        output_processor=TakeFirst()
    )
    