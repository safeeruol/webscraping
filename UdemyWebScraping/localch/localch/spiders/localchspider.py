import scrapy


class LocalchspiderSpider(scrapy.Spider):
    name = 'localchspider'
    allowed_domains = ['local.ch']
    start_urls = ['http://www.local.ch/en/q?what=Coaching&where=Switzerland&rid=DT-w&slot=tel']

    def parse(self, response):
        for item in response.xpath("//div[@class='js-entry-card-container row lui-margin-vertical-xs lui-sm-margin-vertical-m']"):
            title=item.xpath(".//h2/text()").get()
            address=item.xpath(".//div[@class='card-info-address']/span/text()").get()
            categoires=item.xpath(".//div[@class='card-info-category']/span/text()").getall()
            phone_number=item.xpath(".//span[@class='hidden-xs']/text()").get()
            email=item.xpath(".//a[@title='E-Mail']/@href").get()
            website=item.xpath(".//a[@title='Website']/@href").get()

            yield{
                'Title':title,
                'Address':address,
                'Categoires':categoires,
                'Phone_number':phone_number,
                'Email':email,
                'Website':website
            }


            
        next_page = response.xpath("//a[@rel='next']/@href").get()
        if next_page is not None:
            next_page = response.urljoin(next_page)
            yield scrapy.Request(next_page, callback=self.parse) 

