from http.client import ImproperConnectionState
import imp
import scrapy
from ..items import SteamItem
import json
from scrapy.selector import Selector
from scrapy.loader import ItemLoader

class BestSellingSpider(scrapy.Spider):
    name = 'best_selling'
    allowed_domains = ['store.steampowered.com']
    start_urls = ['http://store.steampowered.com/search/results/?query&start=0&count=100&dynamic_data=&sort_by=_ASC&snr=1_7_7_7000_7&filter=topsellers&infinite=1']

    def get_platform(self, list_classes):
        platforms=[]
        for item in list_classes:
            platform=item.split(' ')[-1]
            if platform=='win':
                platforms.append('Windows')
            elif platform=='linux':
                platforms.append('Linux')
            elif platform=='mac':
                platforms.append('Mac OS')
            elif platform=='vr_supported':
                platforms.append('VR Supported')
        return platforms
                


    def parse(self, response):
        content=json.loads(response.body)
        games_content=content.get("results_html")
        games_selector=Selector(text=games_content)
        # steam_item= SteamItem()
        games=games_selector.xpath("//a[@data-gpnav='item']")
        for game in games:
            loader=ItemLoader(item=SteamItem(),selector=game, response=response)
            loader.add_xpath("game_url",".//@href")
            loader.add_xpath("img_url",".//div[@class='col search_capsule']/img/@src")
            loader.add_xpath("game_name",".//div[@class='col search_name ellipsis']/span/text()")
            loader.add_xpath("release_date",".//div[@class='col search_released responsive_secondrow']/text()")
            loader.add_xpath("plateforms",".//span[contains(@class,'platform_img') or @class='vr_supported']/@class")
            loader.add_xpath("discount_rate",".//div[@class='col search_discount responsive_secondrow']/span/text()")
            loader.add_xpath("orignal_price",".//div[@class='col search_price discounted responsive_secondrow']/span/strike/text()")
            loader.add_xpath("dicount_price",".//div[contains(@class, 'search_price discounted')]/text()")
            loader.add_xpath("reviews_summary",".//span[contains(@class, 'search_review_summary')]/@data-tooltip-html")

            yield loader.load_item()
            
            # steam_item['game_url']=game.xpath(".//@href").get()
            # steam_item['img_url']=game.xpath(".//div[@class='col search_capsule']/img/@src").get()
            # steam_item['game_name']=game.xpath(".//div[@class='col search_name ellipsis']/span/text()").get()
            # steam_item['release_date']=game.xpath(".//div[@class='col search_released responsive_secondrow']/text()").get()
            # steam_item['plateforms']=self.get_platform(game.xpath(".//span[contains(@class,'platform_img') or @class='vr_supported']/@class").getall())
            # steam_item['discount_rate']=game.xpath(".//div[@class='col search_discount responsive_secondrow']/span/text()").get()
            # steam_item['orignal_price']=game.xpath(".//div[@class='col search_price discounted responsive_secondrow']/span/strike/text()").get()
            # steam_item['dicount_price']=game.xpath(".//div[contains(@class, 'search_price discounted')]/text()").getall()

           
            # yield steam_item

