# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import imp
from platform import release
import scrapy
from scrapy.loader.processors import TakeFirst, MapCompose
from w3lib.html import remove_tags

def get_platforms(one_class):
    platforms = []

    platform = one_class.split(' ')[-1]
    if platform == 'win':
        platforms.append('Windows')
    if platform == 'mac':
        platforms.append('Mac os')
    if platform == 'linux':
        platforms.append('Linux')
    if platform == 'vr_supported':
        platforms.append('VR Supported')

    return platforms

def clean_discount_rate(discount_rate):
    if discount_rate:
        return discount_rate.lstrip('-')
    return discount_rate

def clean_discounted_price(discounted_price):
    if discounted_price:
        return discounted_price.strip()

def remove_html(review_summary):
    cleaned_review_summary = ''
    try:
        cleaned_review_summary = remove_tags(review_summary)
    except TypeError:
        cleaned_review_summary = 'No reviews'

    return cleaned_review_summary

class SteamItem(scrapy.Item):
    game_url=scrapy.Field(
         output_processor=TakeFirst()
    )
    img_url=scrapy.Field(
        output_processor=TakeFirst()
    )
    game_name=scrapy.Field(
        output_processor=TakeFirst()
    )
    release_date=scrapy.Field(
        output_processor=TakeFirst()
    )
    plateforms=scrapy.Field(
        input_processor=MapCompose(get_platforms)
    )
    rating=scrapy.Field(
        output_processor=TakeFirst()
    )
    orignal_price=scrapy.Field(
        output_processor=TakeFirst()
    )
    dicount_price=scrapy.Field(
        input_processor=MapCompose(clean_discounted_price),
        output_processor = TakeFirst()
    )
    discount_rate=scrapy.Field(
        input_processor=MapCompose(clean_discount_rate),
        output_processor=TakeFirst()
    )
    reviews_summary=scrapy.Field(
        input_processor=MapCompose(remove_html),
        output_processor=TakeFirst()
    )