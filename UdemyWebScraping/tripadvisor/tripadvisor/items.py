# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader.processors import TakeFirst

class TripadvisorItem(scrapy.Item):
    res_id=scrapy.Field(
         output_processor=TakeFirst()
    )
    res_name =scrapy.Field(
    )
    res_sponsor  =scrapy.Field(
         output_processor=TakeFirst()
    )
    res_review  =scrapy.Field(
         output_processor=TakeFirst()
    )
    res_type  =scrapy.Field(
         output_processor=TakeFirst()
    )
    res_price  =scrapy.Field(
         output_processor=TakeFirst()
    )
    res_url  =scrapy.Field(
         output_processor=TakeFirst()
    )

class RestaurantRevies(scrapy.Item):
      com_id =scrapy.Field(output_processor=TakeFirst())
      com_rate =scrapy.Field()
      com_title  =scrapy.Field(output_processor=TakeFirst())
      com_date  =scrapy.Field(output_processor=TakeFirst())
      com_help =scrapy.Field(output_processor=TakeFirst())
      com_url =scrapy.Field(output_processor=TakeFirst())
      com_clientname =scrapy.Field(output_processor=TakeFirst())
      com_clientcom  =scrapy.Field(output_processor=TakeFirst())
      
