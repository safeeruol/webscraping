from pkg_resources import yield_lines
import scrapy
from scrapy.loader import ItemLoader
from ..items import TripadvisorItem, RestaurantRevies

from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor

class RestaurantSpider(scrapy.Spider):
    name = 'restaurant'
    allowed_domains = ['tripadvisor.com']
    start_urls = ['https://www.tripadvisor.com/Restaurants-g60763-New_York_City_New_York.html']
    # rules = (Rule(LinkExtractor(allow=(), restrict_xpaths=('//a[@class="nav next rndBtn ui_button primary taLnk"]/@href',)), callback="parse", follow= True),)


    def parse(self, response):
        for restaurant in response.xpath("//div[contains(@class, 'zdCeB Vt o')]"):
            # loader=ItemLoader(item=TripadvisorItem(),selector=restaurant, response=response)
            # loader.add_xpath('res_name','.//a[@class="Lwqic Cj b"]/text()')
            # loader.add_xpath('res_sponsor','.//span[@class="biGQs _P pZUbB osNWb"]/text()')
            # loader.add_xpath('res_review','.//span[@class="IiChw"]/text()')
            # loader.add_xpath('res_type','.//div[@class="nrKLE PQvPS bAdrM"]//span[@class="qAvoV"][1]//span/text()')
            # loader.add_xpath('res_price','.//div[@class="nrKLE PQvPS bAdrM"]//span[@class="qAvoV"][2]//span/text()')
            # loader.add_xpath('res_url','.//a[@class="Lwqic Cj b"]/@href')
            detail_page_url=restaurant.xpath('.//a[@class="Lwqic Cj b"]/@href').extract_first()
            # yield loader.load_item()

            if detail_page_url is not None:
                yield scrapy.Request(response.urljoin(detail_page_url),callback=self.parse_details)

        # next_page_url = response.xpath('//a[@class="nav next rndBtn ui_button primary taLnk"]/@href').extract_first()

        # if next_page_url is not None:
        #     yield scrapy.Request(response.urljoin(next_page_url))


    def parse_details(self, response):
        for review in response.xpath("//div[@class='review-container']"):
            loader=ItemLoader(item=RestaurantRevies(),selector=review, response=response)
            loader.add_xpath('com_id','.//span[@class="reviews_header_count"]')
            loader.add_xpath('com_rate','.//span[contains(@class,"ui_bubble_rating")]/text()')
            loader.add_xpath('com_title','.//span[@class="noQuotes"]/text()')
            loader.add_xpath('com_date','.//div[@class="prw_rup prw_reviews_stay_date_hsx"]/text()')
            loader.add_xpath('com_help','.//span[@class="numHelp "]/text()')
            loader.add_xpath('com_url','.//a[@class="title "]/@href')
            loader.add_xpath('com_clientname','.//div[@class="info_text pointer_cursor"]/div/text()')
            loader.add_xpath('com_clientcom','.//div[@class="reviewerBadge badge"]/span/text()')
            yield loader.load_item()

        next_page_url = response.xpath('//div[@class="unified ui_pagination "]//a[@class="nav next ui_button primary"]/@href').extract_first()
        if next_page_url is not None:
            yield scrapy.Request(response.urljoin(next_page_url),callback=self.parse_details)

            















