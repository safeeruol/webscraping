import scrapy
from scrapy.loader import ItemLoader
# from demo_project.demo_project.items import QuoteItem


class Goodreads1Spider(scrapy.Spider):
    name = 'goodreads1'
    allowed_domains = ['www.goodreads.com']
    start_urls = ['https://www.goodreads.com/quotes?page=1']

    def parse(self, response):
        for quote in response.xpath("//div[@class='quotes']"):
            # loader=ItemLoader(item=QuoteItem(),selector=quote,response=response)
            # loader.add_xpath('text',".//div[@class='quoteText']/text()[1]")
            # loader.add_xpath('author',".//div[@class='quoteText']/child::a")
            # loader.add_xpath('tags',".//div[@class='greyText smallText left']/a")
            # yield loader.load_item()
            txt=quote.xpath('text',".//div[@class='quoteText']/text()[1]")
            yield txt

        # next_page=response.xpath('//a[@class="next_page"]/@href').extract_first()
        # if next_page is not None:
        #     next_page_link=response.urljoin(next_page)
        #     yield scrapy.Request(url=next_page_link,callback=self.parse)

#//a[@class="next_page"]/@href