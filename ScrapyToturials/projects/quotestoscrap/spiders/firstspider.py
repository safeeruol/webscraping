import scrapy
from scrapy.loader import ItemLoader
from projects.quotestoscrap.items import QuoteItem
class FirstspiderSpider(scrapy.Spider):
    name = 'firstspider'
    allowed_domains = ['quotes.toscrape.com']
    start_urls = ['http://quotes.toscrape.com/']

    def parse(self, response):
        for quote in response.xpath('//div[@class="quote"]'):
            loader = ItemLoader(item=QuoteItem(), selector=quote, response=response)
            loader.add_xpath('text', ".//span[@class='text']/text()")
            loader.add_xpath('author', ".//span[@class='text']/text()")
            yield loader.load_item()
            # text=quote.xpath('.//span[@class="text"]/text()')
            # yield {"title": text}
