import scrapy

class QuotesSpider(scrapy.Spider):
    name = 'quotes'
    start_urls=[
         'https://quotes.toscrape.com/'
    ]

    def Parse(self,response):
        title=response.css('title').extract()
        yield {'titletext',title}