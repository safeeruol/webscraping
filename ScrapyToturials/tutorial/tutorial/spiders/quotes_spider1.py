import scrapy


class QuotesSpider(scrapy.Spider):
    name = "quotes1"
    start_urls=['https://quotes.toscrape.com/page/1/']

    def parse(self, response):
        for quote in response.xpath("//div[@class='quote']"):
            text=quote.xpath(".//span[@class='text']/text()").extract_first()
            author=quote.xpath(".//span/small[@class='author']/text()").get()
            tags=quote.xpath(".//div/a/text()").getall()
            yield {'text':text,'author':author, 'tags':tags}

        next_page= response.xpath("//li[@class='next']/a/@href").get()
        if next_page is not None:
            yield response.follow(next_page,callback=self.parse)