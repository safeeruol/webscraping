import scrapy


class EbookSpider(scrapy.Spider):
    name = "ebook_spider"
    start_urls=['https://www.goodreads.com/shelf/show/ebooks']

    def parse(self, response):
        for ebook in response.xpath("//div[@class='elementList']"):
            bookTitle=ebook.xpath(".//a[@class='bookTitle']/text()").get()
            author=ebook.xpath(".//a[@class='authorName']/span/text()").get()
            rating   = ebook.xpath(".//span[@class='greyText smallText']/text()").get()

            yield {'bookTitle':bookTitle, 'author':author,'rating':rating}

