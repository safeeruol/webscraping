import scrapy


class QuotesSpider(scrapy.Spider):
    name = "quotes2"
    start_urls=['https://www.goodreads.com/quotes/tag/reading?page=1']

    def parse(self, response):
        for quote in response.xpath("//div[@class='quoteDetails ']"):
            text=quote.xpath(".//div[@class='quoteText']/text()").get()
            author=quote.xpath(".//span[@class='authorOrTitle']/text()").get()
            tag=quote.xpath(".//div[@class='greyText smallText left']/a/text()").getall()

            yield {'text':text, 'author':author,'tag':tag}

        next_page= response.xpath("//a[@class='next_page']/@href").get()
        if next_page is not None:
            yield response.follow(next_page,callback=self.parse)