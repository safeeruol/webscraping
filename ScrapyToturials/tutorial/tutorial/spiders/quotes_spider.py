import scrapy


class QuotesSpider(scrapy.Spider):
    name = "quotes"
    start_urls=['https://quotes.toscrape.com/page/1/']
    # def start_requests(self):
    #     urls = [
    #         'https://quotes.toscrape.com/page/1/',
    #     ]
    #     for url in urls:
    #         yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        for quote in response.xpath("//div[@class='quote']"):
            text=quote.xpath(".//span[@class='text']/text()").extract_first()
            author=quote.xpath(".//span/small[@class='author']/text()").get()
            tags=quote.xpath(".//div/a/text()").getall()
            yield {'text':text,'author':author, 'tags':tags}

        next_page= response.xpath("//li[@class='next']/a/@href").get()
        if next_page is not None:
            # next_page=response.urljoin(next_page)
            # yield scrapy.Request(next_page, callback=self.parse)
            yield response.follow(next_page,callback=self.parse)