import scrapy


class EbookUrlSpider(scrapy.Spider):
    name = "ebook_url_spider"
    start_urls=['https://www.goodreads.com/genres/ebooks']

    def parse(self, response):
        for menu in response.xpath("//ul[@class='genreList']/li[@class='genreList__genre']"):
            url=menu.xpath(".//a/@href").get()
            yield response.follow(url=url,callback=self.parse_ebook)


    #
    # def parse_ebook(self,response):




