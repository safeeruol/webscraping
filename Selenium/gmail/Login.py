
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By


driver = webdriver.Chrome('./chromedriver')
driver.get("https://www.python.org")
print(driver.title)

try:
    search_bar = driver.find_element(By.NAME,"q")
except:
    pass

search_bar.clear()
search_bar.send_keys("getting started with python")
search_bar.send_keys(Keys.RETURN)